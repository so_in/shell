#!/bin/sh
#===========================================================================================#
#  Programme pour assigner un password a un utilisateur        03/08/2019
#===========================================================================================#

clear 
echo "Liste des utilisateurs existants (hors système) "
echo ""
#Affiche les utilisateurs en lineblock
echo $(getent passwd | awk -F: '$3 > 1000 && $3 <= 60000 && $6 ~ /^\/home/ {print $1}'/etc/passwd) >/tmp/testuser 
echo ""
username=""
exists=0
#Demander le nom de l utilisateur tant qu'il n'existe pas
until [[ $exists -eq 1 ]] && [ $(grep -w "$username" /tmp/testuser) ]
  do
    read -p "Nom utilisateur dont le mot de passe est à créer (q pour quitter): " username
    #Verifier si l'utilisateur existe, si oui $exists=1 
    exists=$(grep -c "^$username:" /etc/passwd)
    if [[ $exists -eq 0 ]] || [ ! $(grep -w "$username" /tmp/testuser) ] ; then
      #Afficher l'erreur en rouge quand le user est inexistant
      echo -e "\033[31mNom utilisateur invalide ("${username}")\033[0m\n"
    fi
    if [[ $username = q ]] ; then 
      echo "Vous allez quitter le programme" exit 
    fi
done
#Creer un mot de passe pour $username
sudo passwd $username

