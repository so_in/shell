#!/bin/bash
#===========================================================================================# 
#Le programme retire un utilisateur existant de plusieurs groupes existants (04/08/2019)
#===========================================================================================#

echo "Voici la liste des utilisateurs existants (hors système) : "

#Affiche la liste des utilisateurs existants en ligne à la suite les uns des autres.  
echo ""
echo $(getent passwd | awk -F: '$3 > 1000 && $3 <= 60000 {print $1}')

#Demande à l'administrateur le nom de l'utilisateur qu'il souhaite supprimer de plusieurs groupes ou lui demande de saisir la lettre q pour quitter.

user=""
group=""
usersaisi=""
groupsaisi=""
nbgroup=1
	echo ""
	read -p "Quel est le nom de l'utilisateur que vous souhaitez supprimer de plusieurs groupes ? (Si besoin, taper q pour quitter) " user

#Permet de vérifier si l'utilisateur correspond réellement à un utilisateur existant ("usersaisi" sera vide si l'utilisateur n'existe pas).
usersaisi=$(getent passwd $user | awk -F: '$3 > 1000 && $3 <= 60000 {print $1}')

#Tant que l'utilisateur saisi n'existe pas, demande à l'administrateur de saisir un utilisateur valide ou q pour quitter.

	while [[ "$user" != "$usersaisi" ]] && [[ "$user" != "q" ]]
	do 
	read -p "Le nom d'utilisateur saisi n'existe pas. Veuillez resaisir l'utilisateur ou taper q pour quitter : " user
	usersaisi=$(getent passwd $user | awk -F: '$3 > 1000 && $3 <= 60000 {print $1}')
	done
	
#Demande à l'administrateur de combien de groupes différents il souhaite retirer l'utilisateur, et stocke la réponse dans la variable nbgroup. 
	if [[ "$user" != "q" ]]
	then
	read -p "De combien de groupes souhaitez-vous retirer l'utilisateur choisi ? (nb compris entre 1 et 20, ou q pour quitter) :  " nbgroup
	else 
	echo "Vous allez quitter le programme"	
	fi

#Vérifie que le nombre de groupes saisi est bien compris ente 1 et 20 (donc qu'il n'y a pas d'erreur de frappe non plus).
       	if [[ "$nbgroup" != "q" ]]
	then
	while [[ "$nbgroup" -le "0" ]] || [[ "$nbgroup" -gt "20" ]] 
	do
	read -p "Vous devez saisir un nombre de groupes compris entre 1 et 20 : " nbgroup
	done
	elif [[ "$nbgroup" = "q" ]]
	then
	echo "Vous allez quitter le programme"
	fi
#Affiche la liste des groupes existants (hors système), si l'utilisateur et le nombre de groupes saisis sont valides.

        echo""
        if [[ "$user" = "$usersaisi" ]] && [[ "$user" != "q" ]]
        then echo "Voici la liste des groupes existants (hors système) : "
        echo ""
        echo $(getent group | awk -F: '$3 > 1000 && $3 <= 60000 {print $1}')
        echo""
        fi
#Si l'utilisateur correspond à un utilisateur existant et si la saisie est différente de q, demande le groupe duquel  il souhaite retirer l'utilisateur. Le groupe est récupéré dans la variable "group". Toute cette partie est executée le nombre de fois saisi par l'administrateur.

while [[ "$nbgroup" -gt "0" ]] && [[ "$user" != "q" ]] && [[ "$group" != "q" ]]
do
	if [[ "$user" = "$usersaisi" ]] && [[ "$user" != "q" ]]
	then
	read -p "Quel est le nom du groupe au sein duquel vous souhaitez retirer l'utilisateur choisi ? " group
	elif [[ "$user" = "q" ]]
	then
	echo "Vous allez quitter le programme"
	fi

#Permet de vérifier si le groupe correspond réellement à un groupe existant "groupsaisi" sera vide si le groupe n'existe pas).

groupsaisi=$(getent group $group | awk -F: '$3 > 1000 && $3 <= 60000 {print $1}')

#Tant que le groupe saisi n'existe pas, demande à l'admnistrateur de saisir un groupe valide ou q pour quitter. 
	if [[ "$user" != "q" ]]
	then
	while [[ "$group" != "$groupsaisi" ]] && [[ "$group" != "q" ]]
	do 
	read -p "Le nom de groupe saisi n'existe pas. Veuillez saisir de nouveau le groupe ou taper q pour quitter : " group
	groupsaisi=$(getent group $group | awk -F: '$3 > 1000 && $3 <= 60000 {print $1}')
	done
	fi
#Si le groupe existe, effectue la suppression de l'utilisateur du groupe après que l'adminisatrateur ait saisi son mot de passe. Cela l'indique dans le prompt.

if [[ "$group" = "$groupsaisi" ]] 
        then
        sudo gpasswd -d $usersaisi $groupsaisi  &>>/dev/null
        let coderetour=$?
        if [[ $coderetour = 0 ]]
        then
        echo "La suppression de l'utilisateur $usersaisi du groupe $groupsaisi a bien été effectuée."
        elif [[ $coderetour != 0 ]]
        then
        echo "La suppression de l'utilisateur $usersaisi du groupe $groupsaisi a échouée. Il est probable que l'utiisateur $usersaisi ne faisait pas partie du groupe $groupsaisi."
        fi

fi


#Permet de déterminer le nombre de suppression  de l'utilisateur dans un groupe qu'il reste à effectuer. 
nbgroup=$(($nbgroup - 1))
done

#Si l'administrateur saisit q à la place du nom du groupe, cela quitte le programme.

if [[ "$group" = "q" ]]
	then
	echo "Vous allez quitter le programme"
fi

