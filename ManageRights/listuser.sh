#!/bin/bash 

#Script pour lister les utilisateurs d'un groupe
groupsaisi=""
groupe=""


read -p "saisir le nom de groupe : " groupe

#Permet de lister les utilisateurs d'un groupe
list=$(grep "$groupe" /etc/group | cut -d':' -f4) 2>>/dev/null


#Vérifie que le groupe existe
groupsaisi=$(getent group $groupe | cut -d ":" -f1)

if [[ "$groupe" != "$groupsaisi" ]]
then
echo "le groupe n'existe pas" 

else

echo $list
fi

   
