#!/bin/bash

#===========================================================================================#
# Le programme renomme un utilisateur existant       05/08/2019
#===========================================================================================#

nom=""
nouveaunom=""
regex="^[a-zA-Z]+[a-zA-Z0-9]+$"
user=""
usernosys=""
coderetour=""
choixcreation=""
nomconverti=""
nouveaunomconverti=""

echo "Liste des utilisateurs existants (hors système)"
echo ""
echo $(getent passwd | awk -F: '$3 > 1000 && $3 <= 60000 && $6 ~ /^\/home/ {print $1}') #Affiche les utilisateurs en lineblock
echo ""

read -p "Entrer le nom de l'utilisateur à modifier (q pour quitter) " nom

nomconverti=$(echo $nom | iconv -f utf8 -t ascii//TRANSLIT)

#On cherche si l'utilisateur à changer existe 
user=$( cut -d: -f1 /etc/passwd | grep -w "^$nomconverti$" ) 2>>/dev/null
#On crée un fichier temporaire contenant les utilisateurs non système (ID compris entre 1000 et 60000 et existence d'un home)
awk -F: '$3 > 1000 && $3 <= 60000 && $6 ~ /^\/home/ {print $1}' /etc/passwd >/tmp/testuser

if [ "$nomconverti" = "q" ]
then
    echo "Vous allez quitter le programme"
    exit
fi

if [ "$nomconverti" != "q" ]
then
    until [[ "$nomconverti" =~ $regex ]]  && [[ ! -z "$nomconverti" ]]
    do
        read -p "Nom invalide, réessayer (q pour quitter) " nom
        nomconverti=$(echo $nom | iconv -f utf8 -t ascii//TRANSLIT)
    done
    if [ "$nomconverti" = "q" ]
    then
            echo "Vous allez quitter le programme"
            exit
    elif [ "$nomconverti" != "q" ]
    then        
        #On vérifie que l'utilisateur à changer ne correspond pas à un utilisateur système
        until [ $(grep -w "$user" /tmp/testuser) ]
        do
            read -p "Utilisateur système ou inexistant, modification non autorisée, réessayer (q pour quitter) " nom
            nomconverti=$(echo $nom | iconv -f utf8 -t ascii//TRANSLIT)

            if [ "$nomconverti" = "q" ]
            then
                echo "Vous allez quitter le programme"
                exit
            elif [ "$nomconverti" != "q" ] && [[ "$nomconverti" =~ $regex ]]  && [[ ! -z "$nomconverti" ]]
            then
            user=$( cut -d: -f1 /etc/passwd | grep -w "^$nomconverti$" ) 2>>/dev/null            
            fi
        done
    fi 
fi

read -p "Entrer le nouveau nom (q pour quitter) " nouveaunom

nouveaunomconverti=$(echo $nouveaunom | iconv -f utf8 -t ascii//TRANSLIT)

#Recherche et compte le nombre d'occurence du nom exact 
#User :  égal à 1 pour indiquer qu'une ou plusieurs lignes ont été trouvées.
#        égal à 0 pour indiquer qu'aucune ligne n'a été trouvée. 
nomvalide=$( cut -d: -f1 /etc/passwd | grep -w "^$nouveaunomconverti$" -c ) 2>>/dev/null

if [ "$nouveaunomconverti" = "q" ]
then
    echo "Vous allez quitter le programme"
    exit
fi

if [ "$nouveaunomconverti" != "q" ]
then
    until [[ "$nouveaunomconverti" =~ $regex ]]  && [[ ! -z "$nouveaunomconverti" ]]
    do
    read -p "Nom invalide, réessayer (q pour quitter) " nouveaunom
    nouveaunomconverti=$(echo $nouveaunom | iconv -f utf8 -t ascii//TRANSLIT)
    done

    if [ "$nouveaunomconverti" = "q" ]
    then
        echo "Vous allez quitter le programme"
        exit
    elif [ "$nouveaunomconverti" != "q" ]
    then
        #On vérifie que le nouveau nom utilisateur ne correspond pas à un utilisateur déjà existant 
        until [ "$nomvalide" -eq "0" ] && [ "$nouveaunomconverti" != "q" ] && [[ "$nouveaunomconverti" =~ $regex ]]  && [[ ! -z "$nouveaunomconverti" ]]
        do
            read -p "Utilisateur existant, modification non autorisée, réessayer (q pour quitter) " nouveaunom
            nouveaunomconverti=$(echo $nouveaunom | iconv -f utf8 -t ascii//TRANSLIT)
            nomvalide=$( cut -d: -f1 /etc/passwd | grep -w "^$nouveaunomconverti$" -c ) 2>>/dev/null
        done
        
        if [ "$nouveaunomconverti" = "q" ]
        then
            echo "Vous allez quitter le programme"
            exit
        elif [ "$nouveaunomconverti" != "q" ] && [[ "$nouveaunomconverti" =~ $regex ]]  && [[ ! -z "$nouveaunomconverti" ]]
        then            
            #Changement du nom, et déplacement du répertoire personnel dans un nouveau répertoire qui sera créé sans afficher les sorties de commande
            sudo groupmod -n $nouveaunomconverti $nomconverti  &>/dev/null 
            sudo usermod -l $nouveaunomconverti --move-home --home /home/$nouveaunomconverti $nomconverti  &>/dev/null 
            let coderetour=$?
            #echo $coderetour
                if [ "$coderetour" = 0 ]  
                then   
                    echo -e "Modification effectuée\n"
                    echo -e "Voulez-vous modifier le mot de passe?\n" 
                    echo -e "1- Oui"
                    echo -e "2- Non\n"

                    while [[ $choixcreation != "1" && $choixcreation != "2" ]]
                    do 
                        read -p "Votre choix: " choixcreation
                            if [[ $choixcreation != "1" && $choixcreation != "2" ]]
                            then 
                                echo "Choix incorrect. Veuillez choisir entre les choix 1 ou 2"
                            fi 
                    done

                    if [[ $choixcreation == "1" ]]
                    then 
                        ./changepassword.sh
                    fi 

                    if [[ $choixcreation == "2" ]]
                    then
                        echo "Vous allez quitter le programme"
                        exit
                    fi
                   
                elif [ "$coderetour" != 0 ]
                then
                    echo "La modification a échoué"
                    exit 
                fi
        fi
            
    fi
fi





