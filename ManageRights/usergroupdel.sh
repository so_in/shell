#!/bin/bash
#===========================================================================================# 
#Le programme retire un utilisateur existant d'un groupe existant (03/08/2019)
#===========================================================================================#

user=""
group="" 
usersaisi=""
groupsaisi=""

echo "Voici la liste des utilisateurs existants (hors système) : "

#Affiche la liste des utilisateurs existants en ligne à la suite les uns des autres.  
echo ""
echo $(getent passwd | awk -F: '$3 > 1000 && $3 <= 60000 {print $1}')
echo ""

#Demande à l'administrateur le nom de l'utilisateur qu'il souhaite retirer d'un groupe ou lui demande de saisir la lettre q pour quitter. Récupère la saisie dans la variable "user". 

	read -p "Quel est le nom de l'utilisateur que vous souhaitez retirer d'un groupe ?
Si vous souhaitez quitter, merci de saisir la lettre q : " user

#Permet de vérifier si l'utilisateur correspond réellement à un utilisateur existant ("usersaisi" sera vide si l'utilisateur n'existe pas).
usersaisi=$(getent passwd $user | awk -F: '$3 > 1000 && $3 <= 60000 {print $1}')

#Si l'utilisateur saisi n'existe pas, demande à l'administrateur de saisir un utilisateur valide ou q pour quitter.

	while [[ "$user" != "$usersaisi" ]] && [[ "$user" != "q" ]]
	do 
	read -p "Le nom d'utilisateur saisi n'existe pas. Veuillez resaisir l'utilisateur ou taper q pour quitter : " user
	usersaisi=$(getent passwd $user | awk -F: '$3 > 1000 && $3 <= 60000 {print $1}')
	done
	echo""
#Affiche la liste des groupes existants (hors système), si l'utilisateur saisi est valide.

        if [[ "$user" = "$usersaisi" ]] && [[ "$user" != "q" ]]
        then echo "Voici la liste des groupes existants (hors système) : "
        echo ""
        echo $(getent group | awk -F: '$3 > 1000 && $3 <= 60000 {print $1}')
        echo ""
        fi

#Si l'utilisateur correspond à un utilisateur existant et si la saisie est différente de q, demande le groupe duquel il souhaite retirer l'utilisateur. Le groupe est récupéré dans la variable "group".

	if [[ "$user" = "$usersaisi" ]] && [[ "$user" != "q" ]]
	then
	read -p "Quel est le nom du groupe au sein duquel vous souhaitez retirer l'utilisateur choisi ? " group
	elif [[ "$user" = "q" ]]
	then
	echo "Vous allez quitter le programme"
	fi

#Permet de vérifier si le groupe correspond réellement à un groupe existant "groupsaisi" sera vide si le groupe n'existe pas).

groupsaisi=$(getent group $group | awk -F: '$3 > 1000 && $3 <= 60000 {print $1}')

#Si le groupe saisi n'existe pas, demande à l'admnistrateur de saisir un groupe valide ou q pour quitter. 
	if [[ "$user" != "q" ]]
	then
	while [[ "$group" != "$groupsaisi" ]] && [[ "$group" != "q" ]]
	do 
	read -p "Le nom de groupe saisi n'existe pas. Veuillez saisir de nouveau le groupe ou taper q pour quitter : " group
	groupsaisi=$(getent group $group | awk -F: '$3 > 1000 && $3 <= 60000 {print $1}')
	done
	fi

#Si le groupe existe, effectue la suppression du groupe de l'utilisateur choisi, après que l'adminisatrateur ait saisi son mot de passe. Cela l'indique dans le prompt.

if [[ "$group" = "$groupsaisi" ]] 
	then
	sudo gpasswd -d $usersaisi $groupsaisi  &>>/dev/null
        let coderetour=$?
        if [[ $coderetour = 0 ]]
        then
        echo "La suppression de l'utilisateur $usersaisi du groupe $groupsaisi a bien été effectuée."
	elif [[ $coderetour != 0 ]]
	then
        echo "La suppression de l'utilisateur $usersaisi du groupe $groupsaisi a échouée. Il est probable que l'utiisateur $usersaisi ne faisait pas partie du groupe $groupsaisi."
	fi
 
fi

#Si l'administrateur saisit q à la place du nom du groupe, cela quitte le programme.

if [[ "$group" = "q" ]]
	then
	echo "Vous allez quitter le programme"
fi
