#!/bin/bash

#===========================================================================================# 
# Le programme supprime un utilisateur sans supprimer son répertoire        05/08/2019
#===========================================================================================#

nom=""
user=""
regex="^[a-zA-Z]+[a-zA-Z0-9]+$"
coderetour=""
nomconverti=""

echo "Liste des utilisateurs existants (hors système) "
echo ""
echo $(getent passwd | awk -F: '$3 > 1000 && $3 <= 60000 && $6 ~ /^\/home/ {print $1}') #Affiche les utilisateurs en lineblock
echo ""

read -p "Entrer le nom de l'utilisateur à supprimer (q pour quitter) " nom

nomconverti=$(echo $nom | iconv -f utf8 -t ascii//TRANSLIT)

#On cherche si l'utilisateur à supprimer existe 
user=$( cut -d: -f1 /etc/passwd | grep -w "^$nomconverti$" ) 2>>/dev/null
#On crée un fichier temporaire contenant les utilisateurs non système (ID compris entre 1000 et 60000 et existence d'un home)
awk -F: '$3 > 1000 && $3 <= 60000 && $6 ~ /^\/home/ {print $1}' /etc/passwd >/tmp/testuser

if [ "$nomconverti" = "q" ]
then
    echo "Vous allez quitter le programme"
    exit
fi

if [ "$nomconverti" != "q" ]
then
    until [[ "$nomconverti" =~ $regex ]]  && [[ ! -z "$nomconverti" ]]
    do
        read -p "Nom invalide, réessayer (q pour quitter) " nom
    done
    if [ "$nomconverti" = "q" ]
    then
            echo "Vous allez quitter le programme"
            exit
    elif [ "$nomconverti" != "q" ]
    then        
        #On vérifie que l'utilisateur à changer ne correspond pas à un utilisateur système
        until [ $(grep -w "$user" /tmp/testuser) ] && [ "$nomconverti" != "q" ] && [[ "$nomconverti" =~ $regex ]]  && [[ ! -z "$nomconverti" ]]
        do
            read -p "Utilisateur système ou inexistant, suppression non autorisée, réessayer (q pour quitter) " nom           
              
            if [ "$nomconverti" = "q" ]
            then
                echo "Vous allez quitter le programme"
                exit
            fi
            user=$( cut -d: -f1 /etc/passwd | grep -w "^$nomconverti$" ) 2>>/dev/null
        done
            if [ "$nomconverti" != "q" ] && [[ "$nomconverti" =~ $regex ]]  && [[ ! -z "$nomconverti" ]]
            then 
            #Archive le répertoire de l'utilisateur et n'affiche pas les sorties de commande
            sudo mv /home/$nomconverti /home/$nomconverti.old &>/dev/null   
            #Supprime l'utilisateur et n'affiche pas les sorties de commande
            sudo userdel $nomconverti &>/dev/null 
            let coderetour=$?
            #echo $coderetour
                if [ "$coderetour" = 0 ]  
                then          
                    echo "Utilisateur supprimé"
                    exit    
                elif [ "$coderetour" = 8 ]
                then
                    echo "L'utilisateur est actuellement connecté"
                    exit             
                elif [ "$coderetour" != 0 ] && [ "$coderetour" != 0 ]
                then
                    echo "Suprression non effectuée"
                    exit
                fi
            fi                 
    fi 
fi






