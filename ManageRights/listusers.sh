#!/bin/bash

#===========================================================================================#
# Le programme pour lister tous les utilisateurs              03/08/2019
#===========================================================================================#
clear
echo -e "Liste des utilisateurs existants\n"
echo -e "1- Utilisateurs hors systeme"
echo -e "2- Tous les utilisateurs"
echo -e "3- Quitter\n"

#Demander a l'utilisateur de faire un choix pour l'affichage des utilisateurs 
choix=""
while [[ $choix != "1" && $choix != "2" && $choix != "3" ]]
  do 
    read -p "Votre choix: " choix
    if [[ $choix != "1" && $choix != "2" && $choix != "3" ]]
    then 
    echo "Choix incorrect. Veuillez choisir entre les choix 1 ou 2 ou 3"
    fi 
  done

#Afficher les utilisateurs hors systeme
if [[ $choix == "1" ]]
  then 
  #Affiche les utilisateurs en lineblock
  echo $(getent passwd | awk -F: '$3 > 1000 && $3 <= 60000 && $6 ~ /^\/home/ {print $1}')
fi
#Afficher tous les utilisateurs existants
if [[ $choix == "2" ]]
  then
  cat /etc/passwd | awk -F: '{print $1}'
fi
if [[ $choix == "3" ]] 
  then 
  echo "Vous allez quitter le programme" exit 
fi
