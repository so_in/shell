
#=============================================================#
#       création d'un utilisateur                04/08/2019  #           
#=============================================================#


#!/bin/bash
choixcreation=""
usersaisi=""
user=""
regex="^[a-zA-Z]+[a-zA-Z0-9]+$"
userconverti=""


# demande a l'administrateur de saisir un nom d'utilisateur

read -p "Entrer le nom d'utilisateur que vous souhaitez ajouter ou taper q pour quitter :  " user
if [ "$user" = "q" ]
then
echo "Vous allez quitter le programme"   
exit
fi

#Conversion du nom d'utilisateur saisi s'il contient des accents : 

userconverti=$(echo $user | iconv -f utf8 -t ascii//TRANSLIT)

# vérifier que les noms saisis respectent la regex

until [[ "$userconverti" =~ $regex ]]  
    do  
           read -p "Utilisateur invalide, resaisir le nom de l'utilisateur ou taper q pour quitter. (Le nom d'utilisateur ne peut contenir que des lettres ou des chiffres) :  " user
if [ "$user" = "q" ]
        then
            echo "Vous allez quitter le programme"   
            exit
fi


done

# rechercher tout les noms et verfier s'ils existent ou pas

usersaisi=$(getent passwd $userconverti | cut -d ":" -f1)
	 while [[ "$userconverti" = "$usersaisi" ]]

        do
      
        read -p "Le nom d'utilisateur existe déjà. Veuillez saisir un nouveau nom d'utilisateur ou taper q pour quitter : " user

#Conversion du nom d'utilisateur saisi s'il contient des accents :

userconverti=$(echo $user | iconv -f utf8 -t ascii//TRANSLIT)

usersaisi=$(getent passwd $userconverti | cut -d ":" -f1)

if [ "$user" = "q" ]
        then
            echo "Vous allez quitter le programme"   
            exit
fi



done
# la commande useradd cree l'utilisateur et l'ajoute a la lste des utilisateur dans le repertoire /etc/passwd

   
sudo useradd $userconverti &>>/dev/null
	let coderetour=$?
	if [[ $coderetour = 0 ]]
	then
	echo "La création de l'utilisateur $userconverti a bien été effectuée."
	elif [[ $coderetour != 0 ]]
	then 
	echo "La création de l'utilisateur $userconverti a échouée."
	exit
	fi

#Affiche la liste des utilisateurs existants (non système) en ligne .
	echo ""
	echo $(getent passwd | awk -F: '$3 > 1000 && $3 <= 60000 {print $1}')
	echo ""
echo ""


echo -e "Voulez-vous créer un mot de passe pour cet utilisateur?\n" 

echo -e "1- Oui"

echo -e "2- Non\n"

while [[ $choixcreation != "1" && $choixcreation != "2" ]]

do 
   read -p "Votre choix: " choixcreation

   if [[ $choixcreation != "1" && $choixcreation != "2" ]]

   then 

        echo "Choix incorrect. Veuillez choisir entre les choix 1 ou 2"

   fi 
done

if [[ $choixcreation == "1" ]]

then 

   ./createpassword.sh -dGestionUser

fi 

if [[ $choixcreation == "2" ]]

then

   echo "Vous allez quitter le programme"

   exit
fi

