#===================================================================================# 
#        lister les utilisateurs d'un  groupe                       04/08/2019
#===================================================================================#
#!/bin/bash 
groupe=""
groupsaisi=""


read -p "Entrer le nom du groupe ou taper q pour quitter: " groupe

        if [ "$groupe" = "q" ]
        then

            echo "Vous allez quitter le programme"
            exit
        fi


# Recherche les noms du groupe pour verifier si le groupe existe ou pas
groupsaisi=$(getent group $groupe | awk -F: '$3 >= 1000 && $3 <= 60000 {print $1}')

 while [[ "$groupe" != "$groupsaisi" ]] && [[ "$groupe" != "q" ]]
        do
        read -p "Le nom de groupe saisi n'existe pas ou il s'agit d'un utilisateur système. Veuillez saisir de nouveau le groupe ou taper q pour quitter : " groupe
        groupsaisi=$(getent group $groupe | awk -F: '$3 >= 1000 && $3 <= 60000 {print $1}')
        echo""

        if [ "$groupe" = "q" ]
        then
            echo "Vous allez quitter le programme"
            exit
        fi
        done
# lister et afficher les utilisateur d'un groupe 

list=$(getent group $groupe | cut -d':' -f4) 2>>/dev/null


echo "Les utilisateurs du groupe $groupe sont : "
echo $list


   
