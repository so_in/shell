#!/bin/sh
#===========================================================================================#
# Le programme pour modifier un password a un utilisateur        03/08/2019
#===========================================================================================#

clear
echo "Liste des utilisateurs existants (hors système) "
echo ""
#Affiche les utilisateurs en lineblock
echo $(getent passwd | awk -F: '$3 > 1000 && $3 <= 60000 && $6 ~ /^\/home {print $1}'/etc/passwd) >/tmp/testuser
echo ""
username=""
#$exists=1 : une ligne est trouvée, user existe
#$exists=0 : aucune ligne trouvée, user inexistant
exists=0
#Demander le nom de l utilisateur tant qu'il n'existe pas
until [[ $exists -eq 1 ]] && [ $(grep -w "$username" /tmp/testuser) ]
  do
    read -p "Nom utilisateur dont le mot de passe est à modifier: " username
    #Verifier si $username existe en tant qu'utilisateur
    exists=$(grep -c "^$username:" /etc/passwd)
    if [[ $exists -eq 0 ]] ; then
      #Afficher l'erreur en rouge "033" quand on tape un nom user inexistant
      echo -e "\033[31mNom utilisateur invalide ("${username}")\033[0m\n"
    fi
    if [[ $username = q ]] ; then 
      echo "Vous allez quitter le programme" exit 
    fi
done
#Modifier le password de $username 
sudo passwd $username
