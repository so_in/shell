#!/bin/bash
#===========================================================================================# 
#Le programme ajoute un utilisateur existant à un groupe existant (04/08/2019)
#===========================================================================================#
list_user_group ()
{
echo "Voici la liste des utilisateurs existants (hors système) : "
echo ""

#Affiche la liste des utilisateurs existants en ligne à la suite les uns des autres, à l'exclusion des utilisateurs "système".

echo $(getent passwd | awk -F: '$3 > 1000 && $3 <= 60000 {print $1}')

#Demande à l'administrateur le nom de l'utilisateur qu'il souhaite ajouter à un groupe ou lui demande de saisir la lettre q pour quitter.  Récupère la saisie dans la variable "user". 
}
add_user_group ()
{
user=""
group=""
usersaisi=""
groupsaisi=""
 
	echo ""
	read -p "Quel est le nom de l'utilisateur que vous souhaitez ajouter à un groupe ?
	Si vous souhaitez quitter, merci de saisir la lettre q : " user

#Permet de vérifier si l'utilisateur correspond réellement à un utilisateur existant ("usersaisi" sera vide si l'utilisateur n'existe pas).
usersaisi=$(getent passwd $user | awk -F: '$3 > 1000 && $3 <= 60000 {print $1}')

#Si l'utilisateur saisi n'existe pas, demande à l'administrateur de saisir un utilisateur valide ou q pour quitter.

	while [[ "$user" != "$usersaisi" ]] && [[ "$user" != "q" ]]
	do 
	read -p "Le nom d'utilisateur saisi n'existe pas. Veuillez resaisir l'utilisateur ou taper q pour quitter : " user
	usersaisi=$(getent passwd $user | awk -F: '$3 >= 1000 && $3 <= 60000 {print $1}')
	done

#Affiche la liste des groupes disponibles si l'utilisateur saisi est valide.
	
	echo""
        if [[ "$user" = "$usersaisi" ]] && [[ "$user" != "q" ]]
	then echo  "Voici la liste des groupes existants (hors système) : " 
	echo ""
	echo $(getent group | awk -F: '$3 > 1000 && $3 <= 60000 {print $1}')
	echo""
	fi

#Si l'utilisateur correspond à un utilisateur existant et si la saisie est différente de q, demande le groupe dans lequel il souhaite ajouter l'utilisateur. Le groupe est récupéré dans la variable "group".

	if [[ "$user" = "$usersaisi" ]] && [[ "$user" != "q" ]]
	then
	read -p "Quel est le nom du groupe au sein duquel vous souhaitez ajouter l'utilisateur choisi ? " group
	elif [[ "$user" = "q" ]]
	then
	echo "Vous allez quitter le programme"
	fi

#Permet de vérifier si le groupe correspond réellement à un groupe existant "groupsaisi" sera vide si le groupe n'existe pas).

groupsaisi=$(getent group $group | awk -F: '$3 > 1000 && $3 <= 60000 {print $1}')

#Si le groupe saisi n'existe pas, demande à l'admnistrateur de saisir un groupe valide ou q pour quitter. 
	if [[ "$user" != "q" ]]
	then
	while [[ "$group" != "$groupsaisi" ]] && [[ "$group" != "q" ]]
	do 
	read -p "Le nom de groupe saisi n'existe pas. Veuillez saisir de nouveau le groupe ou taper q pour quitter : " group
	groupsaisi=$(getent group $group | awk -F: '$3 > 1000 && $3 <= 60000 {print $1}')
	echo""
	done
	fi
#Si le groupe existe, effectue l'ajout de l'utilisateur au groupe après que l'adminisatrateur ait saisi son mot de passe. Cela l'indique dans le prompt.

if [[ "$group" = "$groupsaisi" ]] 
	then
	sudo gpasswd -a $usersaisi $groupsaisi &>>/dev/null
        let coderetour=$?
        if [[ $coderetour = 0 ]]
        then
        echo "L'ajout de l'utilisateur $usersaisi au groupe $groupsaisi a bien été effectué."
        elif [[ $coderetour != 0 ]]
        then
        echo  "L'ajout de l'utilisateur $usersaisi au groupe $groupsaisi a échoué."
        fi
fi

#Si l'administrateur saisit q à la place du nom du groupe, cela quitte le programme.

if [[ "$group" = "q" ]]
	then
	echo "Vous allez quitter le programme"
fi

}
#Demander à l'admin s'il souhaite commencer par créer les utilisateurs et les groupes
echo "Liste des cas et actions possibles : "
echo ""
                PS3="Veuillez sélectionner la phrase qui correspond à votre situation : "
                select choix in \
                "L'utilisateur et le groupe concernés existent déjà. Je souhaite uniquement ajouter l'utilisateur au groupe." \
                "L'utilisateur existe mais le groupe n'existe pas. Je souhaite d'abord créer le groupe."  \
                "L'utilisateur n'existe pas mais le groupe existe. Je souhaite d'abord créer l'utilisateur." \
                "Ni l'utilisateur ni le groupe n'existe. Je souhaite d'abord les créer." \
		"Je souhaite quitter le programme. Je tape 5."
                do
        case $REPLY in
1)list_user_group
echo""
add_user_group 
break ;;
2)echo "Commençons par créer un groupe."
echo ""
user=""
usersaisi=""
group=""
groupconverti=""
coderetour=""
regex="^[a-zA-Z]+[a-zA-Z0-9]+$"
read -p "Entrer le nom du groupe que vous souhaitez créer ou taper q pour quitter :  " group
if [ "$group" = "q" ]
then
echo "Vous allez quitter le programme"
exit
fi

#Conversion du nom d'utilisateur saisi s'il contient des accents :

groupconverti=$(echo $group | iconv -f utf8 -t ascii//TRANSLIT)

#Vérification du respect de la regex

until [[ "$groupconverti" =~ $regex ]] && [[ "$groupconverti" != "" ]]
    do
           read -p "Groupe invalide, resaisir le nom de groupe ou taper q pour quitter. (Le nom de groupe doit contenir au moins une lettre et peut contenir des chiffres) :  " group
groupconverti=$(echo $group | iconv -f utf8 -t ascii//TRANSLIT)
if [ "$group" = "q" ]
        then
            echo "Vous allez quitter le programme"
            exit
fi

done

#Création du groupe

        sudo groupadd $groupconverti &>>/dev/null
        let coderetour=$?
        if [[ $coderetour = 0 ]]
        then
        echo "La création du groupe $groupconverti a bien été effectuée. Il apparaît dans la liste ci-dessous."
        elif [[ $coderetour != 0 ]]
        then
	echo "La création du groupe $groupconverti a échouée. Vous ne pouvez pas passer à l'étape suivante."
        exit
        fi

#Affiche la liste des groupes existants (non système) en ligne.
        echo ""
        echo $(getent group | awk -F: '$3 > 1000 && $3 <= 60000 {print $1}')
echo ""
echo "Passons à l'étape d'ajout de l'utilisateur au groupe créé."

echo "Voici la liste des utilisateurs existants (hors système) : "
echo ""

#Affiche la liste des utilisateurs existants en ligne à la suite les uns des autres, à l'exclusion des utilisateurs "système".

echo $(getent passwd | awk -F: '$3 > 1000 && $3 <= 60000 {print $1}')

echo ""
        read -p "Quel est le nom de l'utilisateur que vous souhaitez ajouter au groupe $groupconverti ? 
        Si vous souhaitez quitter, merci de saisir la lettre q : " user

#Permet de vérifier si l'utilisateur correspond réellement à un utilisateur existant ("usersaisi" sera vide si l'utilisateur n'existe pas).
usersaisi=$(getent passwd $user | awk -F: '$3 > 1000 && $3 <= 60000 {print $1}')

#Si l'utilisateur saisi n'existe pas, demande à l'administrateur de saisir un utilisateur valide ou q pour quitter.

        while [[ "$user" != "$usersaisi" ]] && [[ "$user" != "q" ]]
        do
        read -p "Le nom d'utilisateur saisi n'existe pas. Veuillez resaisir l'utilisateur ou taper q pour quitter : " user
        usersaisi=$(getent passwd $user | awk -F: '$3 >= 1000 && $3 <= 60000 {print $1}')
        done

#Ajoute l'utilisateur saisi au groupe crée à la première étape de ce script

        sudo gpasswd -a $usersaisi $groupconverti &>>/dev/null
        let coderetour=$?
        if [[ $coderetour = 0 ]]
        then
        echo "L'ajout de l'utilisateur $usersaisi au groupe $groupconverti a bien été effectué."
        elif [[ $coderetour != 0 ]]
        then
        echo  "L'ajout de l'utilisateur $usersaisi au groupe $groupconverti a échoué."
        fi
break ;;
3)echo "Commençons par créer un utilisateur."
user=""
userconverti=""
group=""
groupsaisi=""
coderetour=""
regex="^[a-zA-Z]+[a-zA-Z0-9]+$"
read -p "Entrer le nom d'utilisateur que vous souhaitez créér ou taper q pour quitter :  " user
if [ "$user" = "q" ]
then
echo "Vous allez quitter le programme"
exit
fi

#Conversion du nom d'utilisateur saisi s'il contient des accents :

userconverti=$(echo $user | iconv -f utf8 -t ascii//TRANSLIT)

#Vérification du respect de la regex et d'une saisie non vide

until [[ "$userconverti" =~ $regex ]] && [[ "$userconverti" != "" ]] 
    do
           read -p "Utilisateur invalide, resaisir le nom de l'utilisateur ou taper q pour quitter. (Le nom d'utilisateur doit contenir au moins une lettre et peut contenir des chiffres) :  " user
userconverti=$(echo $user | iconv -f utf8 -t ascii//TRANSLIT)
if [ "$user" = "q" ]
        then
            echo "Vous allez quitter le programme"
            exit
fi

done

#Création de l'utilisateur

	sudo useradd $userconverti &>>/dev/null
	let coderetour=$?
	if [[ $coderetour = 0 ]]
	then
	echo "La création de l'utilisateur $userconverti a bien été effectuée. Il apparaît dans la liste ci-dessous."
	elif [[ $coderetour != 0 ]]
	then 
	echo "La création de l'utilisateur $userconverti a échouée. Vous ne pouvez pas passer à l'étape suivante."
	exit
	fi

#Affiche la liste des utilisateurs existants (non système) en ligne .
	echo ""
	echo $(getent passwd | awk -F: '$3 > 1000 && $3 <= 60000 {print $1}')
	echo ""
	echo "Passons à l'étape d'ajout de l'utilisateur créé au groupe que vous allez choisir."
	echo""
#Affiche la liste des groupes disponibles 

	echo "Voioi la liste des groupes existants (hors système) : "
	echo ""
	echo $(getent group | awk -F: '$3 > 1000 && $3 <= 60000 {print $1}')
	echo""

#Si l'utilisateur correspond à un utilisateur existant et si la saisie est différente de q, demande le groupe dans lequel il souhaite ajouter l'utilisateur. Le groupe est récupéré dans la variable "group".

	read -p "Quel est le nom du groupe au sein duquel vous souhaitez ajouter l'utilisateur choisi ? " group

#Permet de vérifier si le groupe correspond réellement à un groupe existant "groupsaisi" sera vide si le groupe n'existe pas), ou quitte le programme si group =q

	if [[ $group = "q" ]]
	then
	echo "Vous allez quitter le programme"
	exit
	else
	groupsaisi=$(getent group $group | awk -F: '$3 > 1000 && $3 <= 60000 {print $1}')
	fi
#Si le groupe saisi n'existe pas, demande à l'admnistrateur de saisir un groupe valide ou q pour quitter.
        
        while [[ "$group" != "$groupsaisi" ]] && [[ "$group" != "q" ]]
        do
        read -p "Le nom de groupe saisi n'existe pas. Veuillez saisir de nouveau le groupe ou taper q pour quitter : " group
	if [[ $group = "q" ]]
        then
        echo "Vous allez quitter le programme"
        exit
        else
        groupsaisi=$(getent group $group | awk -F: '$3 > 1000 && $3 <= 60000 {print $1}')
        echo""
	fi
        done

#Si le groupe existe, effectue l'ajout de l'utilisateur au groupe après que l'adminisatrateur ait saisi son mot de passe. Cela l'indique dans le prompt.

if [[ "$group" = "$groupsaisi" ]]
        then
        sudo gpasswd -a $userconverti $groupsaisi &>>/dev/null
        let coderetour=$?
	if [[ $coderetour = 0 ]]
        then
        echo "L'ajout de l'utilisateur $userconverti au groupe $groupsaisi a bien été effectué."
        elif [[ $coderetour != 0 ]]
        then
        echo "L'ajout de l'utilisateur $userconverti au groupe $groupsaisi a échoué."
        fi
fi 
break ;;
4)echo "Commençons par créer un utilisateur."
echo""
user=""
userconverti=""
group=""
groupsaisi=""
coderetour=""
regex="^[a-zA-Z]+[a-zA-Z0-9]+$"
read -p "Entrer le nom d'utilisateur que vous souhaitez créér ou taper q pour quitter :  " user
if [ "$user" = "q" ]
then
echo "Vous allez quitter le programme"
exit
fi

#Conversion du nom d'utilisateur saisi s'il contient des accents :

userconverti=$(echo $user | iconv -f utf8 -t ascii//TRANSLIT)

#Vérifie que les noms saisis respectent la regex et qu'une saisie a été effectuée

until [[ "$userconverti" =~ $regex ]] && [[ "$userconverti" != "" ]]
    do
           read -p "Utilisateur invalide, resaisir le nom de l'utilisateur ou taper q pour quitter. (Le nom d'utilisateur doit contenir au moins une lettre et peut contenir des chiffres) :  " user
userconverti=$(echo $user | iconv -f utf8 -t ascii//TRANSLIT)
if [ "$user" = "q" ]
        then
            echo "Vous allez quitter le programme"
            exit
fi

done

#Création de l'utilisateur

        sudo useradd $userconverti &>>/dev/null
        let coderetour=$?
        if [[ $coderetour = 0 ]]
        then
        echo "La création de l'utilisateur $userconverti a bien été effectuée. Il apparaît dans la liste ci-dessous."
	echo $(getent passwd | awk -F: '$3 > 1000 && $3 <= 60000 {print $1}')
        elif [[ $coderetour != 0 ]]
        then
        echo "La création de l'utilisateur $userconverti a échouée. Vous ne pouvez pas passer à l'étape suivante."
        exit
        fi
echo ""
echo "Passons à la création d'un groupe."
echo ""
read -p "Entrer le nom du groupe que vous souhaitez créer ou taper q pour quitter :  " group
if [ "$group" = "q" ]
then
echo "Vous allez quitter le programme"
exit
fi

#Conversion du nom d'utilisateur saisi s'il contient des accents :

groupconverti=$(echo $group | iconv -f utf8 -t ascii//TRANSLIT)

#Vérifie que les noms saisis respectent la regex et que la saisie n'est pas vide

until [[ "$groupconverti" =~ $regex ]] && [[ "$groupconverti" != "" ]]
    do
           read -p "Groupe invalide, resaisir le nom de groupe ou taper q pour quitter. (Le nom de groupe doit contenir au moins une lettre et peut contenir des chiffres) :  " group
groupconverti=$(echo $group | iconv -f utf8 -t ascii//TRANSLIT)
if [ "$group" = "q" ]
        then
            echo "Vous allez quitter le programme"
            exit
fi

done

#Création du groupe

        sudo groupadd $groupconverti &>>/dev/null
        let coderetour=$?
        if [[ $coderetour = 0 ]]
        then
        echo "La création du groupe $groupconverti a bien été effectuée. Il apparaît dans la liste ci-dessous."
        elif [[ $coderetour != 0 ]]
        then
        echo "La création du groupe $groupconverti a échouée. Vous ne pouvez pas passer à l'étape suivante."
        exit
        fi

#Affiche la liste des groupes existants (non système) en ligne .
        echo ""
        echo $(getent group | awk -F: '$3 > 1000 && $3 <= 60000 {print $1}')
echo ""
sudo gpasswd -a $userconverti $groupconverti &>>/dev/null
        let coderetour=$?
        if [[ $coderetour = 0 ]]
        then
        echo "L'ajout de l'utilisateur $userconverti au groupe $groupconverti a bien été effectué."
        elif [[ $coderetour != 0 ]]
        then
        echo "L'ajout de l'utilisateur $userconverti au groupe $groupconverti a échoué."
        fi

break ;;
5)echo "Vous allez quitter le programme"
break ;;
*)echo "Choix invalide. Saisir de nouveau votre choix ou taper 5 pour quitter" ;;
esac
done
