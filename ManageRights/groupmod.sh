
#===================================================================================# 
# Le script pour modifier le nom d'un  groupe                       04/08/2019
#===================================================================================#
#!/bin/bash
 
newgroupe=""
groupe=""
regex="^[a-zA-Z]+[a-zA-Z0-9]+$"
groupconverti=""
groupsaisi=""




#Affiche la liste des groupes  (non systeme) existants en ligne block.  
echo""
echo $(getent group | awk -F: '$3 > 1000 && $3 <= 60000 {print $1}')
        echo ""



# saisir le nom du groupe que vous souhaitez modifier 

read -p "Entrer le nom du groupe que vous souhaitez modifier : " group

if [ "$group" = "q" ]
        then
            echo "Vous allez quitter le programme"   
            exit
fi


# Recherche les noms du groupe pour verifier si le groupe existe ou pas


groupsaisi=$(getent group $group | cut -d ":" -f1) 2>>/dev/null

         while ! [[ "$group" = "$groupsaisi" ]]

do


        read -p "Le nom du groupe n'existe pas, veuillez resaisir un nouveau nom du groupe que vous souhaitez modifier ou taper q pour quitter : " group
	 
groupsaisi=$(getent group $group | cut -d ":" -f1) 2>>/dev/null

 if [ "$group" = "q" ]
        then 
            echo "Vous allez quitter le programme"   
            exit
	fi

  done
   


read -p "Entrer le nouveau nom du groupe ou taper q pour quitter le programme: " newgroup

       if [ "$newgroup" = "q" ]
        then
            echo "Vous allez quitter le programme"   
            exit
fi




#Conversion du nom du groupe saisi s'il contient des accents : 

newgroupconverti=$(echo $newgroup | iconv -f utf8 -t ascii//TRANSLIT)



#On vérifie que les noms des groupes respectent la regex

until [[ "$newgroupconverti" =~ $regex ]]  
       do
           read -p "Nouveau nom du groupe invalide , resaisir un nouveau nom du groupe ou tapper q pour quitter : " newgroup



 if [ "$newgroup" = "q" ]
        then 
            echo "Vous allez quitter le programme"   
            exit
fi

done

       
# Recherche les noms du groupe pour verifier si le groupe existe ou pas


newgroupsaisi=$(getent group $newgroupconverti | cut -d ":" -f1) 2>>/dev/null


 while  [[ "$newgroupconverti" = "$newgroupsaisi" ]]


        do

        read -p "Le nouveau nom du groupe existe deja. resaisir un nouveau nom du groupe ou taper q pour quitter :" newgroup


#Conversion du nom du groupe saisi s'il contient des accents :

newgroupconverti=$(echo $newgroup | iconv -f utf8 -t ascii//TRANSLIT)

newgroupsaisi=$(getent group $newgroupconverti | cut -d ":" -f1) 2>>/dev/null

 	if [ "$newgroup" = "q" ]
        then 
            echo "Vous allez quitter le programme"   
            exit
	fi

 done


# la commande groupadd effectue l'ajoue du groupe saisi par l'administrateur

sudo groupmod -n  $newgroupconverti  $group  &>>/dev/null
        let coderetour=$?
        if [[ $coderetour = 0 ]]
        then
        echo "La modification du groupe $group a bien été effectuée."
        elif [[ $coderetour != 0 ]]
        then
        echo "La modification du groupe $group a échoué"
        exit
        fi

#Affiche la liste des utilisateurs existants (non système) en ligne .
        echo ""
        echo $(getent group | awk -F: '$3 > 1000 && $3 <= 60000 {print $1}')
        echo ""











