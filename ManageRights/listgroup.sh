#!/bin/bash

#===========================================================================================# 
# Le programme liste les groupes non système       04/08/2019
#===========================================================================================#

group=""
groupnosys=""
coderetour=""
choix=""
choixcreation=""

echo ""
echo -e "Liste des groupes existants\n"
echo -e "1- Groupes hors système"
echo -e "2- Tous les groupes"
echo -e "3- Quitter\n"

while [[ $choix != "1" && $choix != "2" && $choix != "3" ]]
do 
    read -p "Votre choix: " choix
        if [[ $choix != "1" && $choix != "2" && $choix != "3" ]]
        then 
            echo "Choix incorrect. Veuillez choisir entre les choix 1, 2 ou 3"
        fi 
done

if [[ $choix == "1" ]]
then 
    #Recherche tous les groupes hors système et redirige les erreurs dans /dev/null 
    groupnosys=$(getent group | awk -F: '$3 > 1000 && $3 <= 60000 {print $1}') 2>>/dev/null     
    let coderetour=$?
    #echo $coderetour 

    #Shell :  égal à 0 pour indiquer qu'une ou plusieurs lignes ont été trouvées.
    #         égal à 1 pour indiquer qu'aucune ligne n'a été trouvée. 
        
    if [ "$coderetour" = 0 ]
    then      
        echo $groupnosys
    elif [ "$coderetour" = 1 ]   
    then           
        echo "Pas de groupe trouvé"
    fi
fi 

if [[ $choix == "2" ]]
then
    group=$(getent group | awk -F: '{print $1}')
    let coderetour=$?
    #echo $coderetour

    if [ "$coderetour" = 0 ]
    then      
        echo $group
    elif [ "$coderetour" = 1 ]   
    then           
        echo "Pas de groupe trouvé"
    fi
fi

if [[ $choix == "3" ]]
then
    echo "Vous allez quitter le programme"
    exit
fi

echo ""
echo -e "Voulez-vous créer un groupe?\n" 
echo -e "1- Oui"
echo -e "2- Non\n"

while [[ $choixcreation != "1" && $choixcreation != "2" ]]
do 
    read -p "Votre choix: " choixcreation
        if [[ $choixcreation != "1" && $choixcreation != "2" ]]
        then 
            echo "Choix incorrect. Veuillez choisir entre les choix 1 ou 2"
        fi 
done

if [[ $choixcreation == "1" ]]
then 
   ./groupadd.sh
fi 

if [[ $choixcreation == "2" ]]
then
    echo "Vous allez quitter le programme"
    exit
fi

