#!/bin/bash

#===========================================================================================#
# Le programme vérifie que l'utilisateur est sudoer      06/08/2019
#===========================================================================================#

#On détermine si le user a des privilèges sudoer 
sudo -v &>/dev/null 
let coderetour=$?
#echo $coderetour

if [ "$coderetour" = "0" ]
then 
    echo "Bienvenue sur le menu"  
    exit
else
    echo "Connectez-vous avec des droits sudoer"
fi