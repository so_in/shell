#!/bin/bash
#===========================================================================================# 
# Le programme change le propriétaire d'un dossier (et des sous-répertoires et fichiers si demandé, et le groupe associé si demandé)                                                                      
#===========================================================================================#

directory=""
response=""
pathdirectory=""
choix=""
know=""
#Demande si l'administrateur connaît le chemin d'accès du dossier 
	echo -e "\033[32mTout au long de ce programme vous avez la possibilité à chaque saisie demandée de taper q pour quitter.\033[0m"
	read -p "Connaissez-vous le chemin d'accès du dossier sur lequel vous souhaitez effectuer des modifications de propriété ? :(o ou n ou q) " know 
	if [[ "$know" = "q" ]]
	then
	echo -e "\033[31mVous allez quitter le programme\033[0m"
	exit
	elif [[ "$know" = "o" ]]
	then 
		read -p "Merci de saisir le chemin d'accès exact du dossier sur lequel vous souhaitez effectuer des modifications de propriété :  " pathdirectory
		while [[ "$pathdirectory" != "q" ]] && [[ "$pathdirectory" = "" ]]
        	        do
                	read -p "Vous n'avez rien saisi. Veuillez saisir un nom de dossier ou taper q pour quitter : " pathdirectory
                	done
		if [[ "$pathdirectory" = "q" ]]
        	then
       		echo "Vous allez quitter le programme"
		fi
                echo ""
choix_possibles ()
{
                echo "Liste des actions possibles : "
                PS3="Quelle action voulez-vous effectuer ? "
                echo ""
                select choix in \
                "Changer le nom du propriétaire du dossier" \
                "Changer le nom du propriétaire du dossier ainsi que le nom du groupe"  \
                "Effectuer l'action 1 et ce aussi sur les sous-dossiers et fichiers" \
                "Effectuer l'action 2 et ce aussi sur les sous-dossiers et fichiers" \
                "Quitter le programme"
                do
                echo ""
        	case $REPLY in
      1)echo "Liste des utilisateurs existants : "
        echo ""
        echo $(getent passwd | awk -F: '$3 > 1000 && $3 <= 60000 {print $1}')
        echo ""
        read -p "Quel est le nom du nouveau propriétaire du dossier ? " owner
        echo ""

#Permet de vérifier si l'utilisateur correspond réellement à un utilisateur existant ("ownersaisi" sera vide si l'utilisateur n'existe pas).
ownersaisi=$(getent passwd $owner | awk -F: '$3 > 1000 && $3 <= 60000 {print $1}')

#Si l'utilisateur saisi n'existe pas, demande à l'administrateur de saisir un utilisateur valide ou q pour quitter.

        while [[ "$owner" != "$ownersaisi" ]] && [[ "$owner" != "q" ]]
        do
        read -p "Le nom d'utilisateur saisi n'existe pas. Veuillez resaisir l'utilisateur ou taper q pour quitter : " owner
        ownersaisi=$(getent passwd $owner | awk -F: '$3 > 1000 && $3 <= 60000 {print $1}')
        done

#Change le propriétaire du dossier choisi. Si l'action a pu être effectuée (chemin correct), affiche la liste des dossiers pour prouver que cela a fonctionné. Sinon indique que l'action a échouée.
        sudo chown $owner $pathdirectory &>>/dev/null
        let coderetour=$?
        if [[ $coderetour = 0 ]]
        then
        echo -e "\033[32mLe propriétaire du dossier a bien été changé.\033[0m"
        sudo ls -al $pathdirectory
        elif [[ $coderetour != 0 ]]
        then
	echo -e "\033[31mLe propriétaire du dossier n'a pu être changé. Vérifiez le chemin du dossier.\033[0m"
        echo ""
        fi
        break ;;
      2)echo "Liste des utilisateurs existants : "
        echo ""
        echo $(getent passwd | awk -F: '$3 > 1000 && $3 <= 60000 {print $1}')
        echo ""
        read -p "Quel est le nom du nouveau propriétaire du dossier ? " owner
#Permet de vérifier si l'utilisateur correspond réellement à un utilisateur existant ("ownersaisi" sera vide si l'utilisateur n'existe pas).
	ownersaisi=$(getent passwd $owner | awk -F: '$3 > 1000 && $3 <= 60000 {print $1}')
#Si l'utilisateur saisi n'existe pas, demande à l'administrateur de saisir un utilisateur valide ou q pour quitter.

        while [[ "$owner" != "$ownersaisi" ]] && [[ "$owner" != "q" ]]
        do
        read -p "Le nom d'utilisateur saisi n'existe pas. Veuillez resaisir l'utilisateur ou taper q pour quitter : " owner
        ownersaisi=$(getent passwd $owner | awk -F: '$3 > 1000 && $3 <= 60000 {print $1}')
        done
	if [[ "$owner" = "q" ]]
	then 
	echo "Vous allez quitter le programme"
	exit 
	fi
	echo "Liste des groupes existants : "
        echo ""
        echo $(getent group | awk -F: '$3 > 1000 && $3 <= 60000 {print $1}')
       	echo ""
        read -p "Quel est le nom du groupe auquel appartient le dossier ? " group

#Permet de vérifier si le groupe correspond réellement à un groupe existant ("groupsaisi" sera vide si l'utilisateur n'existe pas).
	groupsaisi=$(getent group  $group | awk -F: '$3 > 1000 && $3 <= 60000 {print $1}')

#Si le groupe saisi n'existe pas, demande à l'administrateur de saisir un groupe valide ou q pour quitter.

        while [[ "$group" != "$groupsaisi" ]] && [[ "$group" != "q" ]]
        do
        read -p "Le nom de groupe saisi n'existe pas. Veuillez resaisir le nom de groupe ou taper q pour quitter : " group
        groupsaisi=$(getent group $group | awk -F: '$3 > 1000 && $3 <= 60000 {print $1}')
        done
	if [[ "$group" = "q" ]]
	then
	echo "Vous allez quitter le programme"
	exit
	fi

#Change le propriétaire du dossier choisi et le nom du groupe associé. Si l'action a pu être effectuée (chemin correct), affiche la liste des dossiers pour prouver que cela a fonctionné. Sinon indique que l'action a échouée.

        sudo chown $owner:$group $pathdirectory &>>/dev/null
        let coderetour=$?
        if [[ $coderetour = 0 ]]
        then
        echo -e "\033[32mLe propriétaire du dossier et le nom du groupe associé ont bien été changés.\033[0m"
        sudo ls -al $pathdirectory
	elif [[ $coderetour != 0 ]]
        then
        echo -e "\033[31mLe propriétaire du dossier et le nom du groupe associé n'ont pas pu être changés. Vérifier le chemin du dossier.\033[0m"
        echo ""
        fi
        break ;;

      3)echo "Liste des utilisateurs existants : "
        echo ""
        echo $(getent passwd | awk -F: '$3 > 1000 && $3 <= 60000 {print $1}')
        echo ""
        read -p "Quel est le nom du nouveau propriétaire du dossier ? " owner
        echo ""

#Permet de vérifier si l'utilisateur correspond réellement à un utilisateur existant ("ownersaisi" sera vide si l'utilisateur n'existe pas).
        ownersaisi=$(getent passwd $owner | awk -F: '$3 > 1000 && $3 <= 60000 {print $1}')

#Si l'utilisateur saisi n'existe pas, demande à l'administrateur de saisir un utilisateur valide ou q pour quitter.

        while [[ "$owner" != "$ownersaisi" ]] && [[ "$owner" != "q" ]]
        do
        read -p "Le nom d'utilisateur saisi n'existe pas. Veuillez resaisir l'utilisateur ou taper q pour quitter : " owner
        ownersaisi=$(getent passwd $owner | awk -F: '$3 > 1000 && $3 <= 60000 {print $1}')
        done

#Change le propriétaire du dossier choisi ainsi que des sous-dossiers. Si l'action a pu être effectuée (chemin correct), affiche la liste des dossiers pour prouver que cela a fonctionné. Sinon indique que l'action a échouée.
        sudo chown -R $owner $pathdirectory &>>/dev/null
        let coderetour=$?
        if [[ $coderetour = 0 ]]
        then
        echo -e "\033[32mLe propriétaire du dossier et des sous-dossiers qu'il contient a bien été changé.\033[0m"
        sudo ls -al $pathdirectory
        elif [[ $coderetour != 0 ]]
        then
        echo -e "\033[31mLe propriétaire du dossier et des sous-dossiers qu'il contient n'a pu être changé. Vérifier le chemin du dossier.\033[0m"
        echo ""
        fi
        break ;;
      4)echo "Liste des utilisateurs existants : "
        echo ""
        echo $(getent passwd | awk -F: '$3 > 1000 && $3 <= 60000 {print $1}')
        echo ""
	read -p "Quel est le nom du nouveau propriétaire du dossier ? " owner
        echo "Liste des groupes existants : "
        echo ""
        echo $(getent group | awk -F: '$3 > 1000 && $3 <= 60000 {print $1}')
        echo ""
        read -p "Quel est le nom du groupe auquel appartient le dossier ? " group
#Permet de vérifier si l'utilisateur correspond réellement à un utilisateur existant ("ownersaisi" sera vide si l'utilisateur n'existe pas).
ownersaisi=$(getent passwd $owner | awk -F: '$3 > 1000 && $3 <= 60000 {print $1}')
groupsaisi=$(getent group  $group | awk -F: '$3 > 1000 && $3 <= 60000 {print $1}')


#Si l'utilisateur saisi n'existe pas, demande à l'administrateur de saisir un utilisateur valide ou q pour quitter.

        while [[ "$owner" != "$ownersaisi" ]] && [[ "$owner" != "q" ]]
        do
        read -p "Le nom d'utilisateur saisi n'existe pas. Veuillez resaisir l'utilisateur ou taper q pour quitter : " owner
        ownersaisi=$(getent passwd $owner | awk -F: '$3 > 1000 && $3 <= 60000 {print $1}')
        done

        while [[ "$group" != "$groupsaisi" ]] && [[ "$group" != "q" ]]
        do
        read -p "Le nom de groupe saisi n'existe pas. Veuillez resaisir le nom de groupe ou taper q pour quitter : " group
        groupsaisi=$(getent group $group | awk -F: '$3 > 1000 && $3 <= 60000 {print $1}')
        done

#Change le propriétaire du dossier choisi et le nom du groupe associé. Si l'action a pu être effectuée (chemin correct), affiche la liste des dossiers pour prouver que cela a fonctionné. Sinon indique que l'action a échouée.

        sudo chown -R $owner:$group $pathdirectory &>>/dev/null
        let coderetour=$?
        if [[ $coderetour = 0 ]]
        then
        echo -e "\033[32mLe propriétaire du dossier et des sous-dossiers a bien été changé, ainsi que les groupes associés.\033[0m"
        sudo ls -al $pathdirectory
        elif [[ $coderetour != 0 ]]
        then
        echo -e "\033[31mLe propriétaire du dossier et des sous-dossiers n'ont pas pu être changés. Il en est de même pour les groupes associés. Vérifier le chemin du dossier.\033[0m"
        echo ""
        fi
        break ;;

      5)echo -e "\033[31mVous allez quitter le programme.\033[0m"
	 break ;;
      *)echo -e "\033[31mChoix invalide\033[0m"  ;;
   esac
done
}
choix_possibles		
	elif [[ "$know" = "n" ]]
	then	
#Demande de saisir le nom du dossier dont le propriétaire doit être changé. 

read -p "Merci de saisir tout ou partie du nom du dossier pour lequel vous souhaitez effectuer des modifications de propriété : " directory

saisie_dossier ()
{
	if [[ "$directory" != "q" ]] && [[ "$directory" != "" ]]
        then
        sudo find /home -iname "*"$directory"*" -type d
        elif [[ "$directory" = "q" ]]
        then
        echo "Vous allez quitter le programme"
	exit
	else
		while [[ "$directory" != "q" ]] && [[ "$directory" = "" ]] 
        	do
        	read -p "Vous n'avez rien saisi. Veuillez saisir un nom de dossier ou taper q pour quitter : " directory
		done
	
	if [[ "$directory" = "q" ]]
	then
	echo "Vous allez quitter le programme"
	exit
	fi
	fi
}
saisie_dossier

presence_dossier ()
{	

	if [[ "$directory" != "" ]]
	then
	read -p "Le dossier dont vous souhaitez modifier le propriétaire apparaît-il dans les résultats ci-dessus ? (o ou n ou q) : " response
	
	response_dossier ()
	{
		if [[ "$response" = "n" ]]
		then 
		read -p "Vous avez la possibilité de saisir de nouveau un nom de dossier ou taper q pour quitter : " directory
		saisie_dossier 
		presence_dossier
		response_dossier
		elif [[ "$response" = "o" ]]
		then
		read -p "Vous pouvez maintenant recopier le chemin du dossier concerné ici : " pathdirectory
		elif [[ "$response" = "q" ]]
                then
                echo "Vous allez quitter le programme."
		exit
		else
			while [[ "$response" != "q" ]] && [[ "$response" != "o" ]] && [[ "$response" != "n" ]]
                	do
                	read -p "Veuilez répondre à la question par o ou n ou taper q pour quitter : " response
			response_dossier
                	done
	
		fi

	}
	fi
} 
presence_dossier
response_dossier
	echo "Liste des actions possibles : "
                PS3="Quelle action voulez-vous effectuer ? "
                echo ""
                select choix in \
                "Changer le nom du propriétaire du dossier" \
                "Changer le nom du propriétaire du dossier ainsi que le nom du groupe"  \
                "Effectuer l'action 1 et ce aussi sur les sous-dossiers et fichiers" \
                "Effectuer l'action 2 et ce aussi sur les sous-dossiers et fichiers" \
                "Quitter le programme"
                do
                echo ""
                case $REPLY in
      1)echo "Liste des utilisateurs existants : "
        echo ""
        echo $(getent passwd | awk -F: '$3 > 1000 && $3 <= 60000 {print $1}')
        echo ""
        read -p "Quel est le nom du nouveau propriétaire du dossier ? " owner
        echo ""

#Permet de vérifier si l'utilisateur correspond réellement à un utilisateur existant ("ownersaisi" sera vide si l'utilisateur n'existe pas).
ownersaisi=$(getent passwd $owner | awk -F: '$3 > 1000 && $3 <= 60000 {print $1}')

#Si l'utilisateur saisi n'existe pas, demande à l'administrateur de saisir un utilisateur valide ou q pour quitter.

        while [[ "$owner" != "$ownersaisi" ]] && [[ "$owner" != "q" ]]
        do
        read -p "Le nom d'utilisateur saisi n'existe pas. Veuillez resaisir l'utilisateur ou taper q pour quitter : " owner
        ownersaisi=$(getent passwd $owner | awk -F: '$3 > 1000 && $3 <= 60000 {print $1}')
        done

#Change le propriétaire du dossier choisi. Si l'action a pu être effectuée (chemin correct), affiche la liste des dossiers pour prouver que cela a fonctionné. Sinon indique que l'action a échouée.
        sudo chown $owner $pathdirectory &>>/dev/null
        let coderetour=$?
        if [[ $coderetour = 0 ]]
        then
        echo -e "\033[32mLe propriétaire du dossier a bien été changé.\033[0m"
        sudo ls -al $pathdirectory
        elif [[ $coderetour != 0 ]]
        then	
	 echo -e "\033[31mLe propriétaire du dossier n'a pu être changé. Vérifiez le chemin du dossier.\033[0m"
        echo ""
        fi
        break ;;
      2)echo "Liste des utilisateurs existants : "
        echo ""
        echo $(getent passwd | awk -F: '$3 > 1000 && $3 <= 60000 {print $1}')
        echo ""
        read -p "Quel est le nom du nouveau propriétaire du dossier ? " owner
#Permet de vérifier si l'utilisateur correspond réellement à un utilisateur existant ("ownersaisi" sera vide si l'utilisateur n'existe pas).
        ownersaisi=$(getent passwd $owner | awk -F: '$3 > 1000 && $3 <= 60000 {print $1}')
#Si l'utilisateur saisi n'existe pas, demande à l'administrateur de saisir un utilisateur valide ou q pour quitter.

        while [[ "$owner" != "$ownersaisi" ]] && [[ "$owner" != "q" ]]
        do
        read -p "Le nom d'utilisateur saisi n'existe pas. Veuillez resaisir l'utilisateur ou taper q pour quitter : " owner
        ownersaisi=$(getent passwd $owner | awk -F: '$3 > 1000 && $3 <= 60000 {print $1}')
        done
        if [[ "$owner" = "q" ]]
        then
        echo "Vous allez quitter le programme"
        exit
        fi
        echo "Liste des groupes existants : "
        echo ""
        echo $(getent group | awk -F: '$3 > 1000 && $3 <= 60000 {print $1}')
        echo ""
        read -p "Quel est le nom du groupe auquel appartient le dossier ? " group

#Permet de vérifier si le groupe correspond réellement à un groupe existant ("groupsaisi" sera vide si l'utilisateur n'existe pas).
        groupsaisi=$(getent group  $group | awk -F: '$3 > 1000 && $3 <= 60000 {print $1}')

#Si le groupe saisi n'existe pas, demande à l'administrateur de saisir un groupe valide ou q pour quitter.

        while [[ "$group" != "$groupsaisi" ]] && [[ "$group" != "q" ]]
        do
        read -p "Le nom de groupe saisi n'existe pas. Veuillez resaisir le nom de groupe ou taper q pour quitter : " group
        groupsaisi=$(getent group $group | awk -F: '$3 > 1000 && $3 <= 60000 {print $1}')
        done
        if [[ "$group" = "q" ]]
        then
        echo "Vous allez quitter le programme"
        exit
	fi

#Change le propriétaire du dossier choisi et le nom du groupe associé. Si l'action a pu être effectuée (chemin correct), affiche la liste des dossiers pour prouver que cela a fonctionné. Sinon indique que l'action a échouée.

        sudo chown $owner:$group $pathdirectory &>>/dev/null
        let coderetour=$?
        if [[ $coderetour = 0 ]]
        then
        echo -e "\033[32mLe propriétaire du dossier et le nom du groupe associé ont bien été changés.\033[0m"
        sudo ls -al $pathdirectory
        elif [[ $coderetour != 0 ]]
        then
        echo -e "\033[31mLe propriétaire du dossier et le nom du groupe associé n'ont pas pu être changés. Vérifier le chemin du dossier.\033[0m"
        echo ""
        fi
        break ;;

      3)echo "Liste des utilisateurs existants : "
        echo ""
        echo $(getent passwd | awk -F: '$3 > 1000 && $3 <= 60000 {print $1}')
        echo ""
        read -p "Quel est le nom du nouveau propriétaire du dossier ? " owner
        echo ""

#Permet de vérifier si l'utilisateur correspond réellement à un utilisateur existant ("ownersaisi" sera vide si l'utilisateur n'existe pas).
        ownersaisi=$(getent passwd $owner | awk -F: '$3 > 1000 && $3 <= 60000 {print $1}')

#Si l'utilisateur saisi n'existe pas, demande à l'administrateur de saisir un utilisateur valide ou q pour quitter.

        while [[ "$owner" != "$ownersaisi" ]] && [[ "$owner" != "q" ]]
        do
        read -p "Le nom d'utilisateur saisi n'existe pas. Veuillez resaisir l'utilisateur ou taper q pour quitter : " owner
        ownersaisi=$(getent passwd $owner | awk -F: '$3 > 1000 && $3 <= 60000 {print $1}')
        done

#Change le propriétaire du dossier choisi ainsi que des sous-dossiers. Si l'action a pu être effectuée (chemin correct), affiche la liste des dossiers pour prouver que cela a fonctionné. Sinon indique que l'action a échouée.
        sudo chown -R $owner $pathdirectory &>>/dev/null
        let coderetour=$?
        if [[ $coderetour = 0 ]]
        then
        echo -e "\033[32mLe propriétaire du dossier et des sous-dossiers qu'il contient a bien été changé.\033[0m"
	sudo ls -al $pathdirectory
        elif [[ $coderetour != 0 ]]
        then
        echo -e "\033[31mLe propriétaire du dossier et des sous-dossiers qu'il contient n'a pu être changé. Vérifier le chemin du dossier.\033[0m"
        echo ""
        fi
        break ;;
      4)echo "Liste des utilisateurs existants : "
        echo ""
        echo $(getent passwd | awk -F: '$3 > 1000 && $3 <= 60000 {print $1}')
        echo ""
        read -p "Quel est le nom du nouveau propriétaire du dossier ? " owner
        echo "Liste des groupes existants : "
        echo ""
        echo $(getent group | awk -F: '$3 > 1000 && $3 <= 60000 {print $1}')
        echo ""
        read -p "Quel est le nom du groupe auquel appartient le dossier ? " group
#Permet de vérifier si l'utilisateur correspond réellement à un utilisateur existant ("ownersaisi" sera vide si l'utilisateur n'existe pas).
ownersaisi=$(getent passwd $owner | awk -F: '$3 > 1000 && $3 <= 60000 {print $1}')
groupsaisi=$(getent group  $group | awk -F: '$3 > 1000 && $3 <= 60000 {print $1}')


#Si l'utilisateur saisi n'existe pas, demande à l'administrateur de saisir un utilisateur valide ou q pour quitter.

        while [[ "$owner" != "$ownersaisi" ]] && [[ "$owner" != "q" ]]
        do
        read -p "Le nom d'utilisateur saisi n'existe pas. Veuillez resaisir l'utilisateur ou taper q pour quitter : " owner
        ownersaisi=$(getent passwd $owner | awk -F: '$3 > 1000 && $3 <= 60000 {print $1}')
        done

        while [[ "$group" != "$groupsaisi" ]] && [[ "$group" != "q" ]]
        do
        read -p "Le nom de groupe saisi n'existe pas. Veuillez resaisir le nom de groupe ou taper q pour quitter : " group
        groupsaisi=$(getent group $group | awk -F: '$3 > 1000 && $3 <= 60000 {print $1}')
        done

#Change le propriétaire du dossier choisi et le nom du groupe associé. Si l'action a pu être effectuée (chemin correct), affiche la liste des dossiers pour prouver que cela a fonctionné. Sinon indique que l'action a échouée.

        sudo chown -R $owner:$group $pathdirectory &>>/dev/null
        let coderetour=$?
        if [[ $coderetour = 0 ]]
        then
	echo -e "\033[32mLe propriétaire du dossier et des sous-dossiers a bien été changé, ainsi que les groupes associés.\033[0m"
        sudo ls -al $pathdirectory
        elif [[ $coderetour != 0 ]]
        then
        echo -e "\033[31mLe propriétaire du dossier et des sous-dossiers n'ont pas pu être changés. Il en est de même pour les groupes associés. Vérifier le chemin du dossier.\033[0m"
        echo ""
        fi
        break ;;

      5)echo -e "\033[31mVous allez quitter le programme.\033[0m"
         break ;;
      *)echo -e "\033[31mChoix invalide\033[0m"  ;;
   esac
done		
	
	else
	echo "Votre saisie n'est pas valide"
	fi
